package at.campus02.nowa.akse2017.oop.pr3;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class ListenerServerRunnable extends AbstractStoppableRunnable {
	
	private SocketAddress socket;
	private List<AbstractStoppableRunnable> runnables = new ArrayList<>();

	public ListenerServerRunnable(SocketAddress socket) {
		super();
		this.socket = socket;
	}

	public void run() {
		ServerSocket server;
		try {
			server = new ServerSocket();
			server.bind(socket);
			server.setSoTimeout(1000);
			while (shouldRun()) {
				try {
					Socket client = server.accept();
					AbstractStoppableRunnable r = new EchoClientRunnable(client);
					runnables.add(r);
					new Thread(r).start();
				} catch (SocketTimeoutException e) {}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		for (AbstractStoppableRunnable r : runnables) {
			r.requestShutdown();
		}
	}
	
}
