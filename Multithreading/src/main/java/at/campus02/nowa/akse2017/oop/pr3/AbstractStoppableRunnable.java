package at.campus02.nowa.akse2017.oop.pr3;

public abstract class AbstractStoppableRunnable implements Runnable {

	private boolean isRunning = true;
	
	public void requestShutdown() {
		isRunning = false;
	}
	
	public boolean shouldRun() {
		return isRunning;
	}
}
