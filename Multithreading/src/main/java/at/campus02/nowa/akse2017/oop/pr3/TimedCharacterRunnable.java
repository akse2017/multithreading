package at.campus02.nowa.akse2017.oop.pr3;

public class TimedCharacterRunnable implements Runnable {
	
	private char buchstabe;
	private boolean run = true;

	public TimedCharacterRunnable(char buchstabe) {
		super();
		this.buchstabe = buchstabe;
	}

	public void run() {
		while (run) {
			System.out.print(buchstabe);
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}
	
	public void stop() {
		run = false;
	}

}
