package at.campus02.nowa.akse2017.oop.pr3;

public class ThreadDemo {

	public static void main(String[] args) {
		TimedCharacterRunnable r1 = new TimedCharacterRunnable('a');
		TimedCharacterRunnable r2 = new TimedCharacterRunnable('b');
		Thread t1 = new Thread(r1);
		Thread t2 = new Thread(r2);
		t1.start();
		t2.start();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		r1.stop();
		

	}

}
