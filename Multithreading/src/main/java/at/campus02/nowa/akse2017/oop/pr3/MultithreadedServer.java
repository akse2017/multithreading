package at.campus02.nowa.akse2017.oop.pr3;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.List;

public class MultithreadedServer {

	public static void main(String[] args) {
		List<AbstractStoppableRunnable> runnables = new ArrayList<>();
		SocketAddress add1 = new InetSocketAddress(InetAddress.getLoopbackAddress(), 1234);
		SocketAddress add2 = new InetSocketAddress(InetAddress.getLoopbackAddress(), 5678);
		AbstractStoppableRunnable r1 = new ListenerServerRunnable(add1);
		AbstractStoppableRunnable r2 = new ListenerServerRunnable(add2);
		runnables.add(r1);
		runnables.add(r2);
		new Thread(r1).start();
		new Thread(r2).start();
		System.out.println("Server startet!");
		try {
			Thread.sleep(20000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (AbstractStoppableRunnable r : runnables) {
			System.out.println("Stoppe runnable: " + r);
			r.requestShutdown();
		}
		
		
		
		
	}

}

