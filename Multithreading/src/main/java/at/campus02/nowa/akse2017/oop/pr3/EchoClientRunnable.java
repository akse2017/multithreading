package at.campus02.nowa.akse2017.oop.pr3;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

public class EchoClientRunnable extends AbstractStoppableRunnable {

	private Socket client;

	public EchoClientRunnable(Socket client) {
		super();
		this.client = client;
	}

	public void run() {
		try {
			client.setSoTimeout(100);
		} catch (SocketException e) {
			return;
		}
		System.out.println(Thread.currentThread().getId() + ": " + client.getInetAddress());
		try (InputStream ins = client.getInputStream(); OutputStream ops = client.getOutputStream()) {

			while (shouldRun()) {
				try {
					int b = ins.read();
					if (b == -1) {
						return;
					}
					System.out.println(Character.toChars(b)[0]);
					ops.write(b);
					ops.flush();
				} catch (SocketTimeoutException e) {
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
